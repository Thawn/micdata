import time
import multiprocessing as mp
import matplotlib
import numpy as np
import unittest
import os
import sys
import gzip
import pickle
try:
    import micdata
except ModuleNotFoundError:
    sys.path.append(os.path.dirname(os.path.dirname(__file__)))
    import micdata


class TestStackIO(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.two_d = (16, 16)
        self.three_d = (5, 16, 16)
        self.four_d = (5, 16, 16, 1)
        self.five_d = (5, 16, 16, 1, 1)
        self.data_dir = os.path.join(os.path.dirname(__file__), 'data')

    def test_check_dimensions(self):
        io = micdata.io.TiffLoader()
        stack = np.zeros(self.three_d)
        stack = io._check_dimensions(stack)
        self.assertIs(stack.dtype, np.dtype('float32'))
        self.assertEqual(stack.shape, self.four_d)
        self.assertTrue(np.array_equal(stack, np.zeros(self.four_d)))
        stack = np.zeros(self.two_d)
        stack = io._check_dimensions(stack)
        self.assertTrue(np.array_equal(stack, np.zeros((1, 16, 16, 1))))
        self.assertRaises(ValueError, io._check_dimensions, np.zeros(self.five_d))
        self.assertRaises(AssertionError, io._check_dimensions, np.zeros((1)))

    def test_tiff_loader(self):
        io = micdata.io.TiffLoader()
        py_stack = io.load(os.path.join(self.data_dir, 'tiff.tif'))
        self.assertEqual(py_stack.shape, self.four_d)
        fiji_stack = io.load(os.path.join(self.data_dir, 'fiji_tiff.tif'))
        self.assertTrue(np.array_equal(py_stack, fiji_stack))
        self.assertEqual(np.min(py_stack), 11071.0)
        self.assertEqual(np.max(py_stack), 27828.0)

    def test_nd2_loader(self):
        io = micdata.io.BioFormatsLoader()
        stack = io.load(os.path.join(self.data_dir, 'nikon.nd2'))
        self.assertEqual(stack.shape, (11, 512, 512, 3))
        self.assertEqual(np.max(stack), 32749.0)

    def test_io_factory(self):
        path = os.path.join('bla', 'test.tif')
        io = micdata.io.IOFactory(path)
        self.assertIsInstance(io.get_loader(), micdata.io.TiffLoader)
        self.assertIsInstance(io.get_writer(), micdata.io.TiffWriter)
        self.assertEqual(io.path, path)
        self.assertEqual((io.dir_name, io.file_name), os.path.split(path))
        self.assertEqual((io.no_ext, io.ext), os.path.splitext(path))
        io = micdata.io.IOFactory('test.tiff')
        self.assertIsInstance(io.get_loader(), micdata.io.TiffLoader)
        io = micdata.io.IOFactory('test.nd2')
        self.assertIsInstance(io.get_loader(), micdata.io.BioFormatsLoader)
        io = micdata.io.IOFactory('test.stk')
        self.assertIsInstance(io.get_loader(), micdata.io.BioFormatsLoader)


class BaseFormattingTest(unittest.TestCase):
    def setUp(self):
        np.random.seed(42)
        self.stack = np.random.normal(size=(30, 128, 128, 1))
        self.expected_input_dim = 4
        self.expected_output_dim = 5
        self.expected_output_shape = (3, 12, 128, 128, 1)
        self.formatter = micdata.formatting.StackSlicer()

    def test_dims(self):
        self.assertEqual(self.formatter.input_dim, self.expected_input_dim)
        self.assertEqual(self.formatter.output_dim, self.expected_output_dim)

    def test_apply(self):
        stack = self.formatter.apply(self.stack)
        self.assertEqual(stack.shape, self.expected_output_shape)

    def test_revert(self):
        stack = self.formatter.apply(self.stack)
        stack = self.formatter.revert(stack)
        self.assertTrue(np.array_equal(self.stack, stack))

    def test_get_output_shape(self):
        self.assertEqual(self.formatter.get_output_shape(self.stack.shape), self.expected_output_shape)

    def test_get_revert_output_shape(self):
        self.assertEqual(self.formatter.get_revert_output_shape(self.expected_output_shape), self.stack.shape)


class TestEmptyFormatter(BaseFormattingTest):
    def setUp(self):
        super().setUp()
        self.expected_input_dim = None
        self.expected_output_dim = None
        self.expected_output_shape = (30, 128, 128, 1)
        self.formatter = micdata.formatting.EmptyFormatter()

    def test_apply(self):
        stack = self.formatter.apply(self.stack)
        self.assertTrue(np.array_equal(stack, self.stack))


class TestSlicer(BaseFormattingTest):
    def setUp(self):
        super().setUp()
        self.formatter = micdata.formatting.StackSlicer()

    def test_apply_remainder(self):
        slicer = micdata.formatting.StackSlicer(slice_len=11)
        stack = slicer.apply(self.stack)
        self.assertEqual(stack.shape, slicer.get_output_shape(self.stack.shape))

    def test_apply_no_padding(self):
        slicer = micdata.formatting.StackSlicer(slice_padding=0)
        stack = slicer.apply(self.stack)
        self.assertEqual(stack.shape, slicer.get_output_shape(self.stack.shape))

    def test_apply_no_padding_remainder(self):
        slicer = micdata.formatting.StackSlicer(slice_len=11, slice_padding=0)
        stack = slicer.apply(self.stack)
        self.assertEqual(stack.shape, slicer.get_output_shape(self.stack.shape))


class TestFormattingPipeline(BaseFormattingTest):
    def setUp(self):
        super().setUp()
        self.expected_output_shape = (12, 12, 76, 76, 1)
        self.formatters = []
        self.formatters.append(micdata.formatting.StackTiler())
        self.formatters.append(micdata.formatting.StackSlicer())
        self.formatter = micdata.formatting.FormattingPipeline(self.formatters)

    def test_dimension_agnostic_formatters(self):
        self.formatters.insert(1, micdata.formatting.StackNormalizer())
        self.formatters.insert(0, micdata.formatting.StackNormalizer())
        self.formatters.append(micdata.formatting.StackNormalizer())
        self.formatter = micdata.formatting.FormattingPipeline(self.formatters)
        self.assertEqual(self.formatter.input_dim, 4)
        self.assertEqual(self.formatter.output_dim, 5)

    def test_incorrect_pipeline(self):
        self.formatters.append(micdata.formatting.StackTiler())
        self.assertRaises(AssertionError, micdata.formatting.FormattingPipeline, self.formatters)


class TestNormalizer(BaseFormattingTest):
    def setUp(self):
        super().setUp()
        self.expected_output_shape = self.stack.shape
        self.expected_input_dim = None
        self.expected_output_dim = None
        self.formatter = micdata.formatting.StackNormalizer()

    def _verify_result(self, result):
        self.assertAlmostEqual(np.percentile(result, self.formatter.parameters['pmin']), 0.0, places=4)
        self.assertAlmostEqual(np.percentile(result, self.formatter.parameters['pmax']), 1.0, places=4)
        self.assertNotAlmostEqual(np.mean(result), 0.0, places=2)
        t = np.zeros((1, 1), dtype=self.formatter.parameters['dtype'])
        self.assertEqual(result.dtype, t.dtype)

    def test_apply(self):
        stack = self.formatter.apply(self.stack)
        self._verify_result(stack)

    def test_apply_clip(self):
        normalizer = micdata.formatting.StackNormalizer(clip=True)
        stack = normalizer.apply(self.stack)
        self.assertAlmostEqual(np.min(stack), 0.0)
        self.assertAlmostEqual(np.max(stack), 1.0)

    def test_apply_dtype(self):
        self.formatter = micdata.formatting.StackNormalizer(dtype=np.float64)
        stack = self.formatter.apply(self.stack)
        self._verify_result(stack)

    def test_revert(self):
        self.assertAlmostEqual(np.mean(self.stack), 0.0, places=2)
        stack = self.formatter.apply(self.stack)
        self.assertNotAlmostEqual(np.mean(stack), 0.0, places=2)
        stack = self.formatter.revert(stack)
        self.assertAlmostEqual(np.mean(stack), 0.0, places=2)
        self.assertTrue(np.allclose(self.stack, stack, atol=1E-6))


class TestStackGaussianBlur(BaseFormattingTest):
    def setUp(self):
        super().setUp()
        self.expected_input_dim = None
        self.expected_output_dim = None
        self.expected_output_shape = (30, 128, 128, 1)
        self.formatter = micdata.formatting.StackGaussianBlur()

    def test_apply(self):
        out = self.formatter.apply(self.stack)
        self.assertGreater(np.std(self.stack), np.std(out))
        self.assertEqual(out.shape, self.stack.shape)

    def test_revert(self):
        self.assertRaises(micdata.formatting.NotReversibleError, self.formatter.revert, self.stack.shape)

    def test_get_revert_output_shape(self):
        self.assertRaises(micdata.formatting.NotReversibleError,
                          self.formatter.get_revert_output_shape, self.expected_output_shape)


class TestStackProjector(BaseFormattingTest):
    def setUp(self):
        super().setUp()
        self.expected_input_dim = 4
        self.expected_output_dim = 3
        self.expected_output_shape = (128, 128, 1)
        self.formatter = micdata.formatting.StackProjector()

    def test_apply(self):
        out = self.formatter.apply(self.stack)
        self.assertEqual(np.median(self.stack[:, 0, 0]), out[0, 0])
        self.assertEqual(out.shape, self.expected_output_shape)

    def test_apply_max(self):
        self.formatter.parameters['function'] = np.max
        out = self.formatter.apply(self.stack)
        self.assertEqual(np.max(self.stack[:, 0, 0]), out[0, 0])
        self.assertEqual(out.shape, self.expected_output_shape)

    def test_revert(self):
        self.assertRaises(micdata.formatting.NotReversibleError, self.formatter.revert, self.stack.shape)

    def test_get_revert_output_shape(self):
        self.assertRaises(micdata.formatting.NotReversibleError,
                          self.formatter.get_revert_output_shape, self.expected_output_shape)


class TestStackSubtractBackground(BaseFormattingTest):
    def setUp(self):
        np.random.seed(42)
        self.stack = np.random.normal(size=(5, 32, 32, 1))
        self.expected_input_dim = 4
        self.expected_output_dim = 4
        self.expected_output_shape = (5, 32, 32, 1)
        self.formatter = micdata.formatting.StackSubtractBackground()
        self.output_data = os.path.join(os.path.dirname(__file__), 'data', 'bg_res.pkl.gz')

    def test_apply(self):
        inp = np.copy(self.stack)
        out = self.formatter.apply(inp)
        self.assertEqual(out.shape, self.expected_output_shape)
        with gzip.open(self.output_data, 'r') as f:
            true_out = pickle.load(f)
        self.assertTrue(np.array_equal(out, true_out))
        # testing revert here to avoid calling format once again
        reverted = self.formatter.revert(out)
        self.assertTrue(np.array_equal(self.stack, reverted))
        self.formatter.parameters['radius'] = 2
        out = self.formatter.apply(inp)
        self.assertGreater(np.mean(true_out), np.mean(out))

    def test_revert(self):
        self.assertRaises(micdata.formatting.NotReversibleError, self.formatter.revert, self.stack.shape)


class TestTiler(BaseFormattingTest):
    def setUp(self):
        super().setUp()
        self.expected_output_shape = (120, 76, 76, 1)
        self.expected_output_dim = 4
        self.formatter = micdata.formatting.StackTiler()

    def test_apply_wrong_dimensions(self):
        self.assertRaises(AssertionError, self.formatter.apply, np.zeros((2, 2, 2)))

    def test_apply_wrong_x(self):
        self.assertRaises(AssertionError, self.formatter.apply, np.zeros((2, 3, 2, 1)))

    def test_apply_wrong_y(self):
        self.assertRaises(AssertionError, self.formatter.apply, np.zeros((2, 2, 5, 1)))

    def test_apply_wrong_tile_num(self):
        self.assertRaises(AssertionError, micdata.formatting.StackTiler, tile_num=5)

    def test_apply_asymmetric(self):
        tiler = micdata.formatting.StackTiler(tile_rows=2, tile_cols=4)
        stack = tiler.apply(self.stack)
        self.assertEqual(stack.shape, tiler.get_output_shape(self.stack.shape))

    def test_apply_wrong_cols(self):
        tiler = micdata.formatting.StackTiler(tile_rows=2, tile_cols=3)
        self.assertRaises(AssertionError, tiler.apply, self.stack)

    def test_apply_wrong_rows(self):
        tiler = micdata.formatting.StackTiler(tile_rows=3, tile_cols=2)
        self.assertRaises(AssertionError, tiler.apply, self.stack)

    def test_apply_tile_num_not_fitting(self):
        tiler = micdata.formatting.StackTiler(tile_num=9)
        self.assertRaises(AssertionError, tiler.apply, self.stack)


class TestScaler(BaseFormattingTest):
    def setUp(self):
        super().setUp()
        self.expected_output_shape = (30, 64, 64, 1)
        self.expected_input_dim = None
        self.expected_output_dim = None
        self.formatter = micdata.formatting.StackScaler(target_dim=self.expected_output_shape[1:3])

    def test_revert(self):
        with self.assertRaises(micdata.formatting.NotReversibleError):
            stack = self.formatter.revert(self.stack)
        stack = self.formatter.apply(self.stack)
        stack = self.formatter.revert(stack)
        self.assertTrue(self.stack.shape, stack.shape)

    def test_get_revert_output_shape(self):
        with self.assertRaises(micdata.formatting.NotReversibleError):
            self.formatter.get_revert_output_shape(self.expected_output_shape)
        _ = self.formatter.apply(self.stack)
        self.assertEqual(self.formatter.get_revert_output_shape(self.expected_output_shape), self.stack.shape)


class TestPerformanceTiler(BaseFormattingTest):
    def setUp(self):
        np.random.seed(42)
        self.num_cpu = mp.cpu_count()
        self.stack = np.random.normal(size=(1, 128, 128, 1))
        self.expected_input_dim = 4
        self.expected_output_dim = 4
        self.expected_output_dim_separate = 5
        if self.num_cpu == 2:
            self.expected_tile_num = self.num_cpu
            self.expected_tile_rows = 1
            self.expected_tile_cols = 2
            self.expected_output_shape_padding = (2, 76, 128, 1)
            self.expected_output_shape = (2, 64, 128, 1)
        elif self.num_cpu == 4:
            self.expected_tile_num = self.num_cpu
            self.expected_tile_rows = 2
            self.expected_tile_cols = 2
            self.expected_output_shape_padding = (4, 76, 76, 1)
            self.expected_output_shape = (4, 64, 64, 1)
        elif self.num_cpu == 6 or self.num_cpu == 8:
            self.expected_tile_num = 6
            self.expected_tile_rows = 2
            self.expected_tile_cols = 3
            self.expected_output_shape_padding = (4, 76, 76, 1)
            self.expected_output_shape = (4, 64, 64, 1)
        elif self.num_cpu == 12:
            self.expected_tile_num = 12
            self.expected_tile_rows = 3
            self.expected_tile_cols = 4
            self.expected_output_shape_padding = (8, 76, 44, 1)
            self.expected_output_shape = (8, 64, 32, 1)
        elif self.num_cpu == 24:
            self.expected_tile_num = 20
            self.expected_tile_rows = 4
            self.expected_tile_cols = 5
            self.expected_output_shape_padding = (16, 44, 44, 1)
            self.expected_output_shape = (16, 32, 32, 1)
        self.formatter = micdata.formatting.PerformanceTiler()

    def test_dims(self):
        self.formatter.parameters['separate_stacks'] = True
        self.assertEqual(self.formatter.output_dim, self.expected_output_dim_separate)

    def test_apply_padding(self):
        self.formatter.parameters['tile_padding'] = (6, 6)
        stack = self.formatter.apply(self.stack)
        self.assertEqual(stack.shape, self.expected_output_shape_padding)

    def test_revert_padding(self):
        self.formatter.parameters['tile_padding'] = (6, 6)
        stack = self.formatter.apply(self.stack)
        stack = self.formatter.revert(stack)
        self.assertTrue(np.array_equal(self.stack, stack))

    def test_revert_before_apply(self):
        self.assertRaises(NotImplementedError, self.formatter.revert, self.stack)

    def test_calc_rows_cols_tiles(self):
        self.assertEqual(self.formatter.parameters['tile_num'], self.expected_tile_num)
        self.assertEqual(self.formatter.parameters['tile_rows'], self.expected_tile_rows)
        self.assertEqual(self.formatter.parameters['tile_cols'], self.expected_tile_cols)
        self.assertTrue(self.formatter.estimated_parameters)
        self.formatter.apply(np.copy(self.stack))
        self.assertEqual(self.formatter.parameters['tile_num'], self.expected_output_shape[0])
        self.assertEqual(self.formatter.parameters['tile_rows'], self.stack.shape[1] / self.expected_output_shape[1])
        self.assertEqual(self.formatter.parameters['tile_cols'], self.stack.shape[2] / self.expected_output_shape[2])

    def test_get_revert_output_shape(self):
        self.assertRaises(NotImplementedError, self.formatter.get_revert_output_shape, self.expected_output_shape)
        self.formatter.apply(np.copy(self.stack))
        self.assertEqual(self.formatter.get_revert_output_shape(self.expected_output_shape), self.stack.shape)

    def test_revert_separate_stacks(self):
        self.formatter.parameters['separate_stacks'] = True
        stack = self.formatter.apply(self.stack)
        self.assertEqual(len(stack.shape), self.formatter.output_dim)
        self.assertEqual(stack.shape, self.formatter.get_output_shape(self.stack.shape))
        stack = self.formatter.revert(stack)
        self.assertTrue(np.array_equal(self.stack, stack))


class TestParallelFlattener(BaseFormattingTest):
    @classmethod
    def setUpClass(cls):
        np.random.seed(42)
        cls.stack = np.random.normal(size=(50, 512, 512, 1))
        cls.expected_input_dim = 4
        cls.expected_output_dim = 3
        cls.expected_output_shape = (512, 512, 1)

    def setUp(self):
        self.formatter = micdata.formatting.ParallelFlattener()
        self.startTime = time.time()

    def tearDown(self):
        t = time.time() - self.startTime
        print('%s: %.3f' % (self.id(), t))

    def test_get_revert_output_shape(self):
        with self.assertRaises(NotImplementedError):
            self.formatter.get_revert_output_shape(self.expected_output_shape)

    def test_revert(self):
        with self.assertRaises(NotImplementedError):
            self.formatter.revert(self.stack)

    def test_single(self):
        np.median(self.stack, axis=0)

    def test_parallel(self):
        self.formatter.apply(self.stack)


class TestStack(unittest.TestCase):
    def setUp(self):
        self.data_dir = os.path.join(os.path.dirname(__file__), 'data')
        self.file_name = 'tiff.tif'
        np.random.seed(42)
        self.stack = micdata.Stack(self.file_name, data=np.random.normal(size=(30, 128, 128, 1)))

    def test_change_file_name(self):
        tempname = '/bla/blub/tiff.tif'
        self.stack.change_file_name(tempname)
        self.assertIsInstance(self.stack._io, micdata.io.IOFactory)
        self.assertEqual(self.stack._io.path, tempname)

    def test_load(self):
        stack = micdata.Stack(os.path.join(self.data_dir, self.file_name))
        self.assertIsNotNone(stack.data)

    def test_load_data_exists(self):
        stack = micdata.Stack(os.path.join(self.data_dir, self.file_name))
        with self.assertWarns(Warning):
            stack.load()

    def test_save(self):
        self.stack.save()
        stack2 = micdata.Stack(self.file_name)
        self.assertTrue(np.allclose(self.stack.data, stack2.data))
        os.remove(self.file_name)

    def test_format(self):
        self.stack.format(micdata.formatting.StackTiler())
        self.assertTrue(self.stack.formatted)

    def test_revert_format(self):
        data = self.stack.data.copy()
        self.stack.format(micdata.formatting.StackTiler())
        self.stack.revert_format()
        self.assertTrue(np.array_equal(self.stack.data, data))
        self.assertFalse(self.stack.formatted)

    def test_format_no_data(self):
        self.stack.data = None
        self.assertRaises(ValueError, self.stack.format, micdata.formatting.StackSlicer())

    def test_revert_format_not_formatted(self):
        self.assertRaises(AssertionError, self.stack.revert_format)

    def test_format_not_formatter(self):
        self.assertRaises(AssertionError, self.stack.format, np.median)


class TestDisplayStacks(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.data_dir = os.path.join(os.path.dirname(__file__), 'data')

    def setUp(self):
        self.stack = micdata.Stack(os.path.join(self.data_dir, 'tiff.tif'))
        np.random.seed(42)

    def test_instantiate(self):
        disp = micdata.display.DisplayStacks([self.stack, self.stack, self.stack])
        self.assertIsInstance(disp.stacks[0], np.ndarray)
        self.assertEqual(len(disp.stacks), len(disp.titles))

    def test_show_thumbnails(self):
        disp = micdata.display.DisplayStacks([self.stack, self.stack])
        disp.show_thumbnails()
        self.assertIsInstance(disp.fig, matplotlib.figure.Figure)

    def test_calculate_thumbnails(self):
        disp = micdata.display.DisplayStacks([self.stack])
        self.assertTrue(np.array_equal(disp.thumbnails[0], disp.stacks[0][round(disp.stacks[0].shape[0] / 2)]))
        disp.calculate_thumbnails(plane=0)
        self.assertTrue(np.array_equal(disp.thumbnails[0], disp.stacks[0][0]))
        disp.calculate_thumbnails(stack_operation=np.min)
        self.assertTrue(np.array_equal(disp.thumbnails[0], np.min(disp.stacks[0])))

    def test_animate(self):
        disp = micdata.display.DisplayStacks([self.stack, self.stack])
        disp.animate()
        self.assertIsInstance(disp.movie, matplotlib.animation.FuncAnimation)


if __name__ == '__main__':
    unittest.main()
